Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: nsis
Upstream-Contact: http://forums.winamp.com/forumdisplay.php?f=65
Source: https://sourceforge.net/projects/nsis

Files: *
Copyright: 2003-2019, NSIS Contributors
  1995-2003, Mark Adler
  1998-2005, Gilles Vollant
  1999, 2002, Aladdin Enterprises.
  1999-2000, 2002, Aladdin Enterprises.
  1999-2019, Nullsoft and Contributors
  1999-2019, Nullsoft, Jeff Doozan and Contributors
  2001-2002, Yaroslav Faybishenko & Justin Frankel
  2001-2005, Koen van de Sande / Van de Sande Productions
  2002, Robert Rainwater
  2002, Robert Rainwater <rrainwater@yahoo.com>
  2002-2019, Amir Szekely <kichik@netvision.net.il> and Contributors
  2002-2019, Amir Szekely <kichik@users.sourceforge.net>
  2002-2019, Nullsoft and Contributors
  2003, Ramon
  2003, Sunil Kamath
  2007-2019, Jim Park
  2009-2019, Jim Park
  2011-2019, Anders Kjersem
  2013-2019, Anders Kjersem
  2014-2019, Anders Kjersem
  2017-2019, Anders Kjersem
  2018-2019, Anders Kjersem
License: Zlib

Files: Source/bzip2/*
Copyright: 1999-2019, Nullsoft and Contributors
License: BSD-3-clause

Files: Contrib/System/Source/Call.S
 Docs/src/bin/halibut/*
 SCons/Tools/crossmingw.py
 SCons/Tools/mstoolkit.py
Copyright: 1999-2001, Simon Tatham and James Aylett.
  1999-2001, Simon Tatham.
  2001-2004, The SCons Foundation
  2004, John Connors
  2008, Thomas Gaugler <thomas@dadie.net>
  2008-2019, NSIS Contributors
  Robert Rainwater
  Amir Szekely
License: Expat

Files: Source/boost/*
Copyright: 1999-2002, boost.org
  2001-2002, Peter Dimov
  2002-2003, Peter Dimov
  2003, Daniel Frey
  2003, Howard Hinnant
  David Abrahams 2002
  Greg Colvin and Beman Dawes 1998, 1999.
License: NTP

Files: Contrib/NSIS?Menu/nsismenu/nslinks.cpp
 Contrib/NSIS?Menu/wx/setup.h
Copyright: (c) 1999 Vaclav Slavik
 (c) Julian Smart
License: wxWindows
               wxWindows Library Licence, Version 3.1
               ======================================
 .
 Copyright (c) 1998-2005 Julian Smart, Robert Roebling et al
 .
 Everyone is permitted to copy and distribute verbatim copies
 of this licence document, but changing it is not allowed.
 .
                      WXWINDOWS LIBRARY LICENCE
    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 .
 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU Library General Public Licence as published by
 the Free Software Foundation; either version 2 of the Licence, or (at your
 option) any later version.
 .
 This library is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 Licence for more details.
 .
 You should have received a copy of the GNU Library General Public Licence
 along with this software, usually in a file named COPYING.LIB.  If not,
 write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 Floor, Boston, MA 02110-1301 USA.
 .
 EXCEPTION NOTICE
 .
 1. As a special exception, the copyright holders of this library give
 permission for additional uses of the text contained in this release of the
 library as licenced under the wxWindows Library Licence, applying either
 version 3.1 of the Licence, or (at your option) any later version of the
 Licence as published by the copyright holders of version 3.1 of the Licence
 document.
 .
 2. The exception is that you may use, copy, link, modify and distribute
 under your own terms, binary object code versions of works based on the
 Library.
 .
 3. If you copy code from files distributed under the terms of the GNU
 General Public Licence or the GNU Library General Public Licence into a
 copy of this library, as this licence permits, the exception does not apply
 to the code that you add in this way.  To avoid misleading anyone as to the
 status of such modified files, you must delete this exception notice from
 such code and/or adjust the licensing conditions notice accordingly.
 .
 4. If you write modifications of your own for this library, it is your
 choice whether to permit this exception to apply to your modifications.  If
 you do not wish that, you must delete the exception notice from such code
 and/or adjust the licensing conditions notice accordingly.

License: BSD-3-clause
 The BSD License
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
 .
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
 .
   * Neither the name of foo nor the names of its
     contributors may be used to endorse or promote products derived from
     this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
 The MIT License
 .
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to
 whom the Software is furnished to do so, subject to the
 following conditions:
 .
 The above copyright notice and this permission notice shall
 be included in all copies or substantial portions of the
 Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

License: NTP
 Permission to copy, use, modify, sell and distribute this software
 is granted provided this copyright notice appears in all copies.
 This software is provided "as is" without express or implied
 warranty, and with no claim as to its suitability for any purpose.

License: Zlib
 The zlib License
 .
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
   1. The origin of this software must not be misrepresented; you must
      not claim that you wrote the original software. If you use this
      software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.
 .
   2. Altered source versions must be plainly marked as such, and must
      not be misrepresented as being the original software.
 .
   3. This notice may not be removed or altered from any source
      distribution.
